import scrapy
import sqlite3
import datetime
import logging
import itertools

###############################################################################################################
class Stock(scrapy.Item):
    ticker = scrapy.Field()
    stock_info = scrapy.Field()
    insider_info = scrapy.Field()
    news = scrapy.Field()
    
    def __repr__(self):
        return repr(self['ticker'])
###############################################################################################################

###############################################################################################################
def calculate_ema(lst, time):
    if not lst:
        return 0
    tod_price = lst[0]
    k = 2 / (time + 1)
    if time == 1:
        return tod_price
    else:
        if (len(lst) > time):
            return tod_price * k + calculate_ema(lst[1:], time-1) * (1 - k)
        else:
            #logging.error("List not long enough!")
            return tod_price
###############################################################################################################

###############################################################################################################
"""
NOTE TO SELF:

It looks like a high PPO would be a sell signal with these calculations,
and presumably the inverse is true for a low PPO

"""
def calculate_ppo(lst, ms, ml, sig):
    """
    Calculate a stock's Percentage Price Oscillator
    lst is the list of closing data 30 days currently
    ms is the shorter of the MACDs
    ml is the longer of the MACDs
    sig is the signal line
    """
    tmp1 = calculate_ema(lst, ms)
    tmp2 = calculate_ema(lst, ml)
    if (tmp2 == 0):
        tmp2 = 1
    ppo = (tmp1 - tmp2)/tmp2 * 100
    signalLine = calculate_ema(lst, sig)
    
    return ppo
###############################################################################################################

###############################################################################################################
def format_value(inpt):
    out = inpt
    # Handle units
    if out[len(out)-1] == '%':
        out = out[:-1]
        out = float(out)
        out /= 100
    elif out[len(out)-1] == 'K':
        out = out[:-1]
        out = float(out)
        out *= 1000
    elif out[len(out)-1] == 'M':
        out = out[:-1]
        out = float(out)
        out *= 1000000
    elif out[len(out)-1] == 'B':
        out = out[:-1]
        out = float(out)
        out *= 1000000000
    return out
###############################################################################################################

###############################################################################################################
def is_float(value):
    try:
        float(value)
        return True
    except ValueError:
        return False
###############################################################################################################




###############################################################################################################
def update_records(stock):
    """
    Each ticker will have its own table as well, looking like this:
    
    (Ticker, Date, PPO, Insider Summary (ie. buy/sell if more than 3 buys/sells), Insider Data (ie. all transactions pulled for today),
        Index, Market Cap, Income, Sales, Book/sh, Cash/sh, Dividend, Dividend %, Employees, Optionable, Shortable, Recom,
        P/E, Forward P/E, PEG, P/S, P/B, P/C, P/FCF, Quick Ratio, Current Ratio, Debt/Eq, LT Debt/Eq, SMA20,
        EPS (ttm), EPS next Y, EPS next Q, EPS this Y, EPS growth next Y, EPS next 5Y, EPS past 5Y, Sales past 5Y, Sales Q/Q, EPS Q/Q, Earnings, SMA 50,
        Insider Own, Insider Trans, Inst Own, Inst Trans, ROA, ROE, ROI, Gross Margin, Oper. Margin, Profit Margin, Payout, SMA 200,
        Shs Outstand, Shs Float, Short Float, Short Ratio, Target Price, 52W Range, 52W High, 52W Low, RSI (14), Rel Volume, Avg Volume, Volume,
        Perf Week, Perf Month, Perf Quarter, Perf Half Y, Perf Year, Perf YTD, Beta, ATR, Volatility, Prev Close, Price, Change)
    """
    params = []
    command = ""

    conn = sqlite3.connect("stocks.db")

    check = "SELECT 1 FROM TICKER_DATA WHERE TICKER = '{0}' AND DATE LIKE '{1}%';".format(stock['ticker'], str(datetime.datetime.now())[:10])
    
    curs = conn.execute(check)
    if curs.fetchone() is not None:
        # Records exist, so just update them
        command = "UPDATE "
        command += "TICKER_DATA"
        command += " SET "
        for item in stock['stock_info']:
            if stock['stock_info'][item] == '-' or stock['stock_info'][item] == "-":
                command += " '%s'" % item
                command += " = "
                command += "'0',"
            else:
                if item:
                    command += " '%s'" % item
                    command += " = "
                    if item == "Index" or item == "Optionable" or item == "Shortable" or item == "Earnings" or item == "Volatility" or item == "52W Range":
                        command += "'%s'," % stock['stock_info'][item]
                    else:
                        out = stock['stock_info'][item].strip()
                        out = format_value(out)
                        command += "'{0}',".format(out)
        
        
        command = command.rstrip(', ')
        command += " WHERE TICKER = '{1}' AND DATE LIKE '{0}%';".format(str(datetime.datetime.now())[:10], stock['ticker'])
        conn.execute(command)
        conn.commit()
    else:
        command = """INSERT INTO """
        command += "TICKER_DATA"
        command += " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"
    
        params.append(stock['ticker'])
        params.append(datetime.datetime.now())
        params.append("0")
        
        stringy = ''.join(str(e) for e in stock['insider_info'])
        
        params.append(int(stringy.count("Buy") - stringy.count("Sale")))
        params.append(stringy)
        
        for item in stock['stock_info']:
            if item == "Index" or item == "Optionable" or item == "Shortable" or item == "Earnings" or item == "Volatility" or item == "52W Range":
                params.append(stock['stock_info'][item])
            elif stock['stock_info'][item] == "-" or stock['stock_info'][item] == '-':
                params.append(0)
            else:
                out = stock['stock_info'][item].strip()
                out = format_value(out)
                params.append(out)
            
        conn.execute(command, params)
        conn.commit()
    
    conn.close()
###############################################################################################################
    
###############################################################################################################
def setup_main():
    """
    Main table looks like:
    (Ticker, Current Price, Total Quantity Owned, Total Purchase Cost, Total Comissions Paid, Total Misc Costs, Date Updated, Date First Purchased)
    
    Watchlist table looks like:
    (Ticker, Current Price, Fair Price, Reason, Date Updated)
    
    Events table looks like:
    (Ticker, Current Price [@ time of transaction], Type [Buy/Sell], Raw Change [Quantity * Price], Commissions, Net Income [Loss], Date Executed)

    """

    conn = sqlite3.connect("stocks.db")
    conn.execute('''CREATE TABLE IF NOT EXISTS MAIN (TICKER TEXT PRIMARY KEY NOT NULL, CURRENTPRICE REAL NOT NULL, TOTALQUANTITYOWNED REAL NOT NULL, TOTALPURCHASECOST REAL NOT NULL, TOTALCOMISSIONSPAID REAL NOT NULL, TOTALMISCCOSTS REAL NOT NULL, DATEUPDATED TEXT NOT NULL, DATEFIRSTPURCHASED TEXT NOT NULL);''')
    
    conn.execute('''CREATE TABLE IF NOT EXISTS WATCHLIST (TICKER TEXT PRIMARY KEY NOT NULL, CURRENTPRICE REAL NOT NULL, FAIRPRICE REAL, REASON TEXT, DATEUPDATED TEXT NOT NULL);''')
    
    conn.execute('''CREATE TABLE IF NOT EXISTS EVENTS (TICKER TEXT NOT NULL, CURRENTPRICE REAL NOT NULL, TYPE TEXT NOT NULL, RAWCHANGE REAL NOT NULL, TOTALCOMMISSIONSPAID REAL NOT NULL, NETINCOME REAL NOT NULL, DATEEXECUTED TEXT NOT NULL);''')
        
    conn.commit()
    
    conn.close()
###############################################################################################################

###############################################################################################################
# This should be run to process a new stock
def setup_ticker(stock):
    conn = sqlite3.connect("stocks.db")
    
    command = """CREATE TABLE IF NOT EXISTS """
    command += "TICKER_DATA"
    command += """ (TICKER TEXT NOT NULL, DATE TEXT NOT NULL, PPO REAL NOT NULL, INSIDERSUMMARY REAL NOT NULL, INSIDERDATA TEXT NOT NULL, """

    for item in stock['stock_info']:
        command += "'%s'" % item
        if item == "Index" or item == "Optionable" or item == "Shortable" or item == "Earnings" or item == "Volatility" or item == "52W Range":
            command += " TEXT NOT NULL,"
        else:
            command += " REAL NOT NULL,"
    
    command = command[:-1]
    command += ");"
    conn.execute(command)
    conn.commit()
    
    conn.close()
###############################################################################################################

###############################################################################################################
def update_field(ticker, field, newval):
    conn = sqlite3.connect("stocks.db")
    item = str(datetime.datetime.now())
    command = "UPDATE TICKER_DATA SET '{1}' = '{2}' WHERE TICKER = '{0}' AND DATE LIKE '{3}%';".format(ticker, field, newval, item[:10])
    conn.execute(command)
    conn.commit()
    conn.close()
###############################################################################################################




###############################################################################################################
class FinSpider(scrapy.Spider):
    name = "finspider"
    site = "http://finviz.com/"
    historical_start = "http://www.nasdaq.com/symbol/"
    historical_end = "/historical"
    
    
    def start_requests(self):
        urls = [
            'http://finviz.com/screener.ashx?v=111&f=cap_microover',
            #'http://finviz.com/screener.ashx?v=111&f=cap_smallover,fa_ps_high,sh_insidertrans_pos,sh_price_u50,targetprice_a5&ft=2',
            ]
        setup_main()
        yield scrapy.Request(url = urls[0], callback = self.parse_screener)
        
        

    def parse_stock(self, response):
        ret = {"a" : "b"}
        
        headings = []
        contents = []
        
        insiders_titles = ["Name", "Relationship", "Date", "Transaction", "Price", "# Shares", "Value ($)", "# Shares Total", "SEC Form 4"]
        
        trading = []
        news = []
        
        # Grab all stock data
        for item in response.css("td.snapshot-td2-cp"):
            headings.append(item.css("::text").extract_first())
        for item in response.css("td.snapshot-td2"):
            contents.append(item.css("::text").extract_first())
        
        # Selling
        for row in response.css("tr.insider-sale-row-1"):
            insider_row = []
            for item in row.css("td"):
                str(insider_row.append(item.css("::text").extract_first()))
            trading.append(zip(insiders_titles, insider_row))
        for row in response.css("tr.insider-sale-row-2"):
            insider_row = []
            for item in row.css("td"):
                str(insider_row.append(item.css("::text").extract_first()))
            trading.append(zip(insiders_titles, insider_row))
        
        # Buying
        for row in response.css("tr.insider-buy-row-1"):
            insider_row = []
            for item in row.css("td"):
                str(insider_row.append(item.css("::text").extract_first()))
            trading.append(zip(insiders_titles, insider_row))
        for row in response.css("tr.insider-buy-row-2"):
            insider_row = []
            for item in row.css("td"):
                str(insider_row.append(item.css("::text").extract_first()))
            trading.append(zip(insiders_titles, insider_row))
        
        # Grab news items
        for item in response.css("a.tab-link-news"):
            news.append((item.css("::text").extract_first(), item.css("::attr(href)").extract_first()))
        
        ret = dict(zip(headings, contents))
        
        nextrequest = scrapy.Request(self.historical_start + response.url.split("=")[1].split("&")[0] + self.historical_end, callback = self.parse_historical_data)
        nextrequest.meta['ticker_label'] = response.url.split("=")[1].split("&")[0]
        yield nextrequest
        
        curStock = Stock({'ticker' : response.url.split("=")[1].split("&")[0], 'stock_info' : ret, 'insider_info' : trading, 'news' : news})
        setup_ticker(curStock)
        update_records(curStock)
        yield curStock
        
        
    def parse_screener(self, response):       
        if response.url == "http://finviz.com/elite.ashx":
            yield None
            return
        
        tickers_list = []
        tickers = []
        
        for item in response.css("td.screener-body-table-nw"):
            cur = item.css("a.screener-link-primary::attr(href)").extract_first()
            if cur is not None:
                tickers_list.append(cur)
        
        # Process each of the tickers on the page
        for cnt in range(len(tickers_list)):
            yield scrapy.Request(self.site + tickers_list[cnt], callback = self.parse_stock)
            
        
        # Process all pages returned by the query
        for item in response.css("a.tab-link"):
            if item.css("::text").extract_first() == "next":
                logging.info("Next page: " + item.css("::attr(href)").extract_first())
                yield scrapy.Request(self.site + item.css("::attr(href)").extract_first(), callback = self.parse_screener)

    
    def parse_historical_data(self, response):        
        adjs = []
        cntr = 0
        
        for item in response.css("div.genTable").css("td"):
            if cntr % 6 == 4:
                adjs.append(item.css("::text").extract_first().strip())
            cntr += 1

        adjs = filter(bool, adjs)
        adjs = [float(x) for x in adjs]
        
        # We want to update PPO
        update_field(response.meta['ticker_label'], "PPO", calculate_ppo(adjs, 12, 26, 9))
        
###############################################################################################################
